function convertCompanyList(companies) {
  return Object.keys(companies).reduce((acc, companyKey) => {
    let company = companies[companyKey];
    company.key = companyKey;
    (company.domains || []).forEach((domain) => {
      acc[domain] = company;
    });
    return acc;
  }, {});
}

const domainToCompany = convertCompanyList(window.companies);

function tabUpdated(tabId, changeInfo, tab) {
  if (!tab.active) {
    browser.pageAction.hide(tabId);
    return;
  }

  const domain = domainForTab(tab);
  const company = domainToCompany[domain];
  if (!company) {
    browser.pageAction.hide(tabId);
    return;
  }

  const title = `${company.name} owns ${domain}`;
  browser.pageAction.setTitle({ tabId: tabId, title: title });
  browser.pageAction.setIcon({ tabId: tabId, path: `icons/${company.key}.png` });
  browser.pageAction.show(tabId);
}

function domainForTab(tab) {
  const url = new URL(tab.url);
  const host = url.hostname;
  return host.split(".").slice(-2).join(".");
}

function pageActionClicked(tab) {
  const domain = domainForTab(tab);
  const company = domainToCompany[domain];
  if (!company) {
    browser.pageAction.hide(tabId);
    return;
  }

  browser.tabs.create({
    url: company.url
  });
}

browser.tabs.onUpdated.addListener(tabUpdated);
browser.pageAction.onClicked.addListener(pageActionClicked);
